package dam.androidjaviersolercanto.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.widget.TextView;

import org.w3c.dom.Text;

public class NewActivity extends AppCompatActivity {

    private TextView tvText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        setUI();
    }

    private void setUI() {
        tvText = findViewById(R.id.tvText);

        Intent intent = getIntent();

        SharedPreferences myPreferences = null;

        switch (intent.getIntExtra("preferences", R.id.rbShared)) {
            case R.id.rbShared:
                myPreferences = getSharedPreferences("MyPrefs",MODE_PRIVATE);
                System.out.println("Shared");
                break;
            case R.id.rbPreferences:
                System.out.println("Preferences");
                myPreferences = getSharedPreferences("MainActivity", MODE_PRIVATE);
                break;
            case R.id.rbDefault:
                System.out.println("Default");
                myPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                break;
        }

        tvText.setText("PLAYER: " + myPreferences.getString("PlayerName","unknown") +
                "\nSCORE: " + myPreferences.getInt("Score",0) +
                "\nLEVEL: " + myPreferences.getInt("Level",0) +
                "\nDIFFICULTY: " + myPreferences.getInt("Difficulty",0) +
                "\nSOUND: " + myPreferences.getBoolean("Sound",true) +
                "\nBACKGROUND: " + myPreferences.getInt("Background",0));
    }
}
